import unsplash from '../apis/unsplash'

export const fetchImages = () => async dispatch => {
    const response = await unsplash.get('/photos/?client_id=cOhenaPBS-jobfZNjIh0_0HTfmt4jA6xt_RrB5F4zoU');

    dispatch({ type: 'FETCH_IMAGES', payload: response.data })
}

export const selectImage = image => {
    return {
        type: 'SELECTED_IMAGE',
        payload: image
    }
}