import React, { useEffect } from 'react';
import { StyleSheet, Text, ScrollView, View, Image, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import { fetchImages, selectImage } from '../actions';

const ImageList = ({ images, selectImage, navigation, fetchImages }) => {

    useEffect(() => {
        fetchImages();
    })

    return (
        <ScrollView contentContainerStyle={styles.container}>
            {images.map(element => {
                return (
                    <View style={styles.images} key={element.id}>
                        <Text style={styles.description}>{element.alt_description}</Text>
                        <Text style={styles.name}>{element.user.first_name}</Text>
                        <TouchableHighlight  style={styles.image} onPress={() => {selectImage(element), navigation.navigate('SelectedImage')}} >
                            <Image  style={styles.image} source={{uri: element.urls.regular}}/>
                        </TouchableHighlight>
                    </View>
                    );
                })
            }
        </ScrollView>
    )
}

const mapStateToProps = state => {
    console.log(state.selectImage)
    return {
        images: state.images
    }
}
export default connect(mapStateToProps, { fetchImages, selectImage })(ImageList);

const styles = StyleSheet.create({
    container: {
      display: 'flex',
      flexDirection: 'column',
      backgroundColor: '#FFF',
      width: '100%',
      height: 2800,
      alignItems: 'center',
      justifyContent: 'space-between'
    },
    images: {
        width: '94%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
        height: 250,
        borderWidth: 1,
        margin: 10
    },
    image: {
        width: 150, 
        height: 150, 
        marginBottom: 5
    },
    name: {
        fontSize: 14
    },
    description: {
        fontSize: 18
    }
  });
