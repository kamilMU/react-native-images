import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { connect } from 'react-redux';

const SelectedImage = ({ image }) => {

  if(!image) {
    return (<View><Text>Select an image</Text></View>);
  }
  return (
      <View> 
        <Image style={styles.img} source={{uri: image.urls.regular}}/>
      </View>
  )

}

const mapStateToProps = state => {
  console.log(state.selectedImage)
  return {
    image: state.selectedImage
  }
}
export default connect(mapStateToProps)(SelectedImage);

const styles = StyleSheet.create({
    img: {
      width: '100%',
      height: '100%',
    }
  });

