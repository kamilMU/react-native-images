export const selectedImageReducer = (selectedImage = null, action) => {
    if(action.type === 'SELECTED_IMAGE') {
        return action.payload
    }
    return selectedImage;
}

export const imagesReducer = (state = [], action) => {
    switch(action.type) {
        case 'FETCH_IMAGES':
            return action.payload;
        default:
            return state;
    }
}
