import { combineReducers } from 'redux';
import { imagesReducer, selectedImageReducer } from './imagesReducer';


export const allReducers = combineReducers({
    images: imagesReducer,
    selectedImage: selectedImageReducer
})

