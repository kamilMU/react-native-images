import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { allReducers } from './reducers'
import thunk from 'redux-thunk'
import ImageList from './components/ImageList';
import SelectedImage from './components/SelectedImage.js';

const Stack = createStackNavigator();
const store = createStore(allReducers, applyMiddleware(thunk))

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="ImageList"
            component={ImageList}
            options={{ title: 'ImageList' }}
          />
          <Stack.Screen 
            name="SelectedImage" 
            component={SelectedImage}
            options={{ title: 'SelectedImage' }} 
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default App;
